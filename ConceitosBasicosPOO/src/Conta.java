
public class Conta {
	//atributos
	int numero;
	double saldo;
	boolean ativo;
	
	//construtor � um m�todo publico 
	public Conta(int numero, double saldo) {
		this.numero = numero;
		this.saldo = saldo;
		this.ativo = false;
	}
	
	//sobrecarregar mesmo nome, parametros diferentes
	public Conta(int numero, double saldo, boolean ativo) {
		this.numero = numero;
		this.saldo = saldo;
		this.ativo = ativo;
	}
	
	//m�todos
	
	public void Sacar(double valor) {
		this.saldo -= valor;
	}
	
	public void Depositar(double valor) {
		this.saldo += valor;
	}
}
